#include <iostream>
#include "Settings.h"

using namespace std;

int main() 
{
	string configName;
	do
	{
		cout<<"Podaj plik konfiguracyjny: ";
		cin>>configName;
		if(configName == "config.cfg")
			Settings::UstawPlikKonfiguracyjny(configName);
		else
			cout<<"Zla nazwa pliku"<<endl;
	}
	while(configName != "config.cfg");
	Settings::UstawPolaczenieZBazaDanych("root");
	Settings::getInstance();
	return 0;
}
