#include <iostream>

using namespace std;

class Settings
{
	public:
		void UstawPolaczenieZBazaDanych(string);
		string connectionString;
		string config;
		static void getInstance();
		void UstawPlikKonfiguracyjny(string);
	private:
		static int counter;
		Settings();
};
int Settings::counter = 0;
Settings::Settings()
{
	config = "C:/config.cfg";
	UstawPlikKonfiguracyjny(config);
}

void Settings::UstawPlikKonfiguracyjny(string x)
{
	config = x;
}

void Settings::UstawPolaczenieZBazaDanych(string connectionString)
{
	cout<<"Polaczono z baza"<<endl;
}

void Settings::getInstance() 
{
	Settings::counter++;
	return getInstance();
}
