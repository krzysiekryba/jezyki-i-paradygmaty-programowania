#include <iostream>
#include <iomanip>
using namespace std;

int rozmiar;
long **tabA, **tabB, **tabC;

template<typename A, typename B, typename C>
C mul(A tabA, B tabB, C tabC)
{
	for(int i=0; i<rozmiar; i++)
	{
	   for(int j=0; j<rozmiar; j++)
	   {
	       tabC[i][j]=0;
	        for(int k=0; k<rozmiar; k++)
	        {
		       tabC[i][j] += tabA[i][k] * tabB[k][j];
	   	    }
	   }
   }
}

int main()
{
	int i,j;
	cout<<"Podaj rozmiar macierzy kwadratowej: ";
	cin>>rozmiar;
	
	tabA = new long* [rozmiar];
	for (unsigned i = 0; i < rozmiar; ++i)
	tabA[i] = new long [rozmiar];
	 
	tabB = new long* [rozmiar];
	for (unsigned i = 0; i < rozmiar; ++i)
	tabB[i] = new long [rozmiar];
	 
	tabC = new long* [rozmiar];
	for (unsigned i = 0; i < rozmiar; ++i)
	tabC[i] = new long [rozmiar];
	
	
	for(i = 0; i < rozmiar; i++)
	   for(j = 0; j < rozmiar; j++)
	      tabA[i][j] = 2;
	for(i = 0; i < rozmiar; i++)
	   for(j = 0; j < rozmiar; j++)
	      tabB[i][j] = 3;
	mul(tabA,tabB,tabC);
	for(i = 0; i < rozmiar; i++)
	{
	   for(j = 0; j < rozmiar; j++)
	      cout<<setw(3)<<tabC[i][j];
		cout<<endl;
	}
	
}
