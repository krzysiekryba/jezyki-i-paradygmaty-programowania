#include <cstdlib>
#include <iostream>

using namespace std;

class Point2D
{
      private:
             double x;
             double y;
      public:
             Point2D(double, double);
             friend ostream & operator<<(ostream & out, const Point2D & p);
             friend istream & operator>>(istream & in, Point2D & p);
             const Point2D operator+ (const Point2D &) const;
             const Point2D operator- (const Point2D &) const;
             const Point2D operator* (const Point2D &) const;
             const Point2D operator/ (const Point2D &) const;
             const bool operator== (const Point2D &) const;
             Point2D operator+= (Point2D &);
             Point2D operator-= (Point2D &);
             Point2D operator*= (Point2D &);
             Point2D operator/= (Point2D &);
             double operator double() const;
};
Point2D::Point2D(double x, double y):x(x), y(y){}

ostream & operator<<(ostream & out, const Point2D & p) 
{
    out<<p.x<<" "<<p.y; // dostep do danych prywatnych
    return out;
}
istream & operator>>(istream & in, Point2D & p) 
{
    cout<<"X: ";
    in>>p.x;
    cout<<"Y: ";
    in>>p.y; // dostep do danych prywatnych
    return in;
}
const Point2D Point2D::operator+ (const Point2D & prawy_operand) const
{
    return Point2D(x + prawy_operand.x, y + prawy_operand.y);
}
const Point2D Point2D::operator- (const Point2D & prawy_operand) const
{
    return Point2D(x - prawy_operand.x, y - prawy_operand.y);
}
const Point2D Point2D::operator* (const Point2D & prawy_operand) const
{
    return Point2D(x * prawy_operand.x, y * prawy_operand.y);
}
const Point2D Point2D::operator/ (const Point2D & prawy_operand) const
{
    return Point2D(x / prawy_operand.x, y / prawy_operand.y);
}

const bool Point2D::operator== (const Point2D & prawy_operand) const
{
	if (x == prawy_operand.x && y == prawy_operand.y)
		return true;
	else
		return false;
}

Point2D Point2D::operator+= (Point2D & prawy_operand) 
{
    return Point2D(x += prawy_operand.x, y += prawy_operand.y);
}

Point2D Point2D::operator-= (Point2D & prawy_operand) 
{
    return Point2D(x -= prawy_operand.x, y -= prawy_operand.y);
}

Point2D Point2D::operator*= (Point2D & prawy_operand) 
{
    return Point2D(x *= prawy_operand.x, y *= prawy_operand.y);
}

Point2D Point2D::operator/= (Point2D & prawy_operand) 
{
    return Point2D(x /= prawy_operand.x, y /= prawy_operand.y);
}
double Point2D::operator double() const;
{
	//obliczenia
	return x;
}

int main(int argc, char *argv[])
{
	
    Point2D p1(11, 7);
    Point2D p2(5, 3);
    Point2D p3(0,0);
    if(p3 == Point2D(0,0))
    	p3-=p1;
    cout<<p3<<endl;
    p3 = p1 + p2;
    cout<<p3<<endl;
    //p3 == Point2D(20,20); // Ma zwr�cia true
    p3 = p1 - p2;
    cout<<p3<<endl;
    p3 = p1 * p2;
    cout<<p3<<endl;
    //p1 += p2;
    //p1 == Point2D(20, 20); // Ma zwr�cia true
    //p2 *= 2;
    //p2 == Point2D(20, 20); // Ma zwr�cia true
    //cout << p1; // Ma wypisaa na ekranie: (20,20)
    //cout<<p3;
    system("PAUSE");
    return EXIT_SUCCESS;
}
