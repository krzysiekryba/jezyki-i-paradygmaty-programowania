#include <iostream>
using namespace std;
template < class typ >
class SmartPointer
{
public:
	typ X;
	typ Y;
    SmartPointer() 
    {
    }
    int show()
    {
        cout<<"X: "<< X<<" Y: "<<Y<<endl;
    }
    ~SmartPointer()
    {
    }
};


int main()
{
    SmartPointer < double >* point = new SmartPointer < double >;
    SmartPointer < double >* point2 = new SmartPointer < double >;
    point->X = 23.6;
    point->Y = 3.3;
    point->show();
    point2->X = 6;
    point2->Y = 2;
    point2->show();
    
 	delete point;
 	delete point2;
 	system("pause");
}
