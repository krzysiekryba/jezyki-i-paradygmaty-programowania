#include <iostream>
#include <windows.h>


using namespace std;

struct punkt
{
	punkt *poprzedni = NULL;
	punkt *nastepny = NULL;
	int wartosc;
	
};


class Lista
{
	public:
		Lista();
		void Push_Front(Lista & L, int x);
		void Push_Back(Lista & L, int x);
		void Iterator_Front(Lista & L);
		void Iterator_Back(Lista & L);
		int Size();
		bool Empty();
		int Front(Lista & L);
		int Back(Lista & L);
		void Pop_Back(Lista & L);
		void Pop_Front(Lista & L);
		void Remove(Lista & L, punkt * e);
	private:
		int size;
		punkt *glowa;
		punkt *ogon;

};

bool Lista::Empty()
{
	if(size == 0)
		return true;
	else
		return false;
}
Lista::Lista()
{
	size = 0;
	glowa = NULL;
	ogon = NULL;
}
int Lista::Front(Lista & L)
{
	punkt * p = new punkt;
	p = glowa;
	return p->wartosc;
}
int Lista::Back(Lista & L)
{
	punkt * p = new punkt;
	p = ogon;
	return p->wartosc;
}


void Lista::Push_Front(Lista & L, int x)
{
	punkt * p = new punkt;
	p->wartosc = x;
	p->poprzedni = NULL;
	p->nastepny = L.glowa;
	L.glowa = p;
	L.size++;
	if(p->nastepny) 
		p->nastepny->poprzedni = p;
  	else 
	  	L.ogon = p;
}
void Lista::Iterator_Front(Lista & L)
{
  punkt * p;

  p = L.glowa;
  for(int i=0;i<size;i++)
  {
    cout<<p->wartosc<<" ";
    p = p->nastepny;
  }
}

int Lista::Size()
{
	return size;
}
void Lista::Push_Back(Lista & L, int x)
{
  punkt * p = new punkt;
  p->wartosc = x;
  p->nastepny = NULL;
  p->poprzedni = L.ogon;
  L.ogon  = p;
  L.size++;
  if(p->poprzedni) 
  	p->poprzedni->nastepny = p;
  else 
  	L.glowa = p;
}
void Lista::Remove(Lista & L, punkt * e)
{
  L.size--;
  if(e->poprzedni) 
  	e->poprzedni->nastepny = e->nastepny;
  else        
  	L.glowa = e->nastepny;
  if(e->nastepny) 
  	e->nastepny->poprzedni = e->poprzedni;
  else        
  	L.ogon = e->poprzedni;
  delete e;
}
void Lista::Pop_Back(Lista & L)
{
	if(L.size) 
		Remove(L,L.ogon);
}
void Lista::Pop_Front(Lista & L)
{
	if(L.size) 
		Remove(L,L.glowa);
}

void Lista::Iterator_Back(Lista & L)
{
	 punkt * p;

	  p = L.ogon;
	  for(int i = size;i>0;i--)
	  {
	    cout<<p->wartosc<<" ";
	    p = p->poprzedni;
	  }
	
}

