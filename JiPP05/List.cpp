#include <iostream>
#include <windows.h>
#include "List.h"

using namespace std;

int main() 
{
	Lista list;
	int wybor, wartosc, liczba;
	do
	{
		
		cout<<"----- Zawartosc listy -----"<<endl;
		cout<<"Od przodu: ";
		list.Iterator_Front(list);
		cout<<endl;
		cout<<"Od tylu:   ";
		list.Iterator_Back(list);
		cout<<endl;
		cout<<"----------------------------"<<endl;
		cout<<"(1) Empty"<<endl;
		cout<<"(2) Size"<<endl;
		cout<<"(3) Front"<<endl;
		cout<<"(4) Back"<<endl;
		cout<<"(5) Push_Back"<<endl;
		cout<<"(6) Pop_Back"<<endl;
		cout<<"(7) Pop_Front"<<endl;
		cout<<"(8) Push_Front"<<endl;
		cout<<"(9) Wyjscie"<<endl;
		cout<<"Wpisz: ";
		cin>>wybor;
		switch(wybor)
		{
			case 1:
				if(list.Empty())
					cout<<"Lista jest pusta";
				else
					cout<<"Lista nie jest pusta";
				Sleep(2000);
				system("cls");
				break;
			case 2:
				cout<<"Rozmiar listy: "<<list.Size();
				Sleep(2000);
				system("cls");
				break;
			case 3:
				cout<<"Front: "<<list.Front(list);
				Sleep(2000);
				system("cls");
				break;
			case 4:
				cout<<"Back: "<<list.Back(list);
				Sleep(2000);
				system("cls");
				break;
			case 5:
				cout<<"Podaj wartosc: ";
				cin>>wartosc;
				list.Push_Back(list, wartosc);
				system("cls");
				break;
			case 6:
				list.Pop_Back(list);
				system("cls");
				break;
			case 7:
				list.Pop_Front(list);
				system("cls");
				break;
			case 8:
				cout<<"Podaj wartosc: ";
				cin>>wartosc;
				list.Push_Front(list, wartosc);
				system("cls");
				break;
		}
		
		
		
		
	
	}
	while(wybor!=9);
	return 0;
}
