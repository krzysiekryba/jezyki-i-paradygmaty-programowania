#include <iostream>
#include <windows.h>

#define MAX 100
using namespace std;




class Queue
{
	private:
		int dane[MAX];
		int size;
		int glowa = 0;
		int ogon = 0;
	public:
		void Push_Back(int a);
		int Front();
		int Back();
		bool Empty();
		int Size();
		void Pop_Front();
		void Show();
		Queue();
		
};
Queue::Queue() 
{
	size = 0;
}
int Queue::Size()
{
	return size;
}
bool Queue::Empty()
{
	if(size == 0)
		return true;
	else
		return false;
}
int Queue::Front()
{
	if(Empty()!=true)
		return dane[glowa];
}
int Queue::Back()
{
	if(Empty()!=true)
		return dane[ogon];
}

void Queue::Pop_Front()
{
	if(Empty()!=true)
	{
		glowa++;
		size--;
	}
	else
	{
		cout<<"Kolejka jest pusta";
		Sleep(1000);
	}
}

void Queue::Push_Back(int a)
{
	if(size!=MAX)
	{
		if(Empty() == true)
		{
			dane[glowa] = a;
			size++;
		}
		else
		{
			ogon++;
			dane[ogon] = a;
			size++;
		}
	}
	else
	{
		cout<<"Kolejka jest pelna";
		Sleep(1000);
	}
}
void Queue::Show()
{
	if(Empty()!=true)
	{
		for(int i = glowa;i<=ogon;i++)
		{
			cout<<" "<<dane[i]<<" ";
		}
	}
}
