#include <iostream>
#include <windows.h>
#include "Queue.h"
#define MAX 100
using namespace std;


int main()
{
	Queue Q;
	int wybor;
	int a;
	do
	{
		cout<<"----- Zawartosc kolejki -----"<<endl;
		Q.Show();
		cout<<endl<<"-----------------------------"<<endl;
		cout<<"(1) Push_Back"<<endl;
		cout<<"(2) Pop_Front"<<endl;
		cout<<"(3) Front"<<endl;
		cout<<"(4) Back"<<endl;
		cout<<"(5) Size"<<endl;
		cout<<"(6) Empty"<<endl;
		cout<<"(7) Wyjscie"<<endl;
		cout<<"Wpisz: ";
		cin>>wybor;
		switch(wybor)
		{
			case 1:
				cout<<"Podaj liczbe: ";
				cin>>a;
				Q.Push_Back(a);
				system("cls");
				break;
			case 2:
				Q.Pop_Front();
				system("cls");
				break;
			case 3:
				if(Q.Empty()!=true )
				{
					cout<<"Front: "<<Q.Front();
					Sleep(2000);
				}
				else
				{
					cout<<"Kolejka jest pusta";
					Sleep(1000);
				}
				system("cls");
				break;
			case 4:
				if(Q.Empty()!=true )
				{
					cout<<"Back: "<<Q.Back();
					Sleep(2000);
				}
				else
				{
					cout<<"Kolejka jest pusta";
					Sleep(1000);
				}
				system("cls");
				break;
			case 5:
				cout<<"Rozmiar: "<<Q.Size();
				Sleep(1000);
				system("cls");
				break;
			case 6:
				if(Q.Empty()==true)
					cout<<"Kolejka jest pusta";
				else
					cout<<"Kolejka nie jest pusta";
				Sleep(1000);
				system("cls");
				break;
				
		}
	}
	while(wybor !=7);
	
	
	
	return 0;
}
